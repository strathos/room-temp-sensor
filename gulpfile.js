var gulp = require("gulp");
var rename = require("gulp-rename");
var concat = require("gulp-concat");
var babel = require("gulp-babel");
var fs = require("fs");

gulp.task("copy", function() {
  if (fs.existsSync("./src/config.js")) {
    return;
  } else {
    gulp
      .src("./src/config.js-template")
      .pipe(rename("config.js"))
      .pipe(gulp.dest("./src/"));
  }
});

gulp.task("bundle", function() {
  return gulp
    .src("./src/*.js")
    .pipe(concat("bundle.js"))
    .pipe(babel())
    .pipe(gulp.dest("./dist/"));
});

gulp.task("default", ["copy", "bundle"], function() {
  gulp.watch("src/*.js", ["bundle"]);
});
