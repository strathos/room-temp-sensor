const wifi = require("Wifi");
const dht = require("DHT22").connect(pin);
const mqtt = require("MQTT").create(mqttHost);
const esp = require("ESP8266");

mqtt.on("connected", () => {
  console.log("Connected to MQTT broker", mqttHost);
  mqtt.publish(mqttConfigTopic, "Sensor name: " + mqttSensorName + " IP: " + wifi.getIP().ip);
  dht.read(a => {
    const temp = a.temp.toString();
    const humid = a.rh.toString();
    console.log("Temp is " + temp + " and RH is " + humid);
    mqtt.publish(mqttTempTopic, temp);
    mqtt.publish(mqttHumidTopic, humid);
    console.log("Going to deep sleep in 5 seconds");
    setTimeout(() => {
      console.log("Deep sleeping now");
      esp.deepSleep(deepSleepDuration, 2);
    }, 5 * 1000);
  });
});

const onInit = () => {
  wifi.stopAP();
  wifi.connect(wifi_ssid, { password: wifi_password }, err => {
    if (err) {
      console.log("Not connected to Wifi. Error: ", err);
    } else {
      console.log("Connected to Wifi. IP address is:", wifi.getIP().ip);
      console.log("Waiting for 5 seconds before connecting to MQTT broker");
      setTimeout(() => {
        console.log("Connecting to MQTT broker");
        mqtt.connect();
      }, 5 * 1000);
    }
  });
};
