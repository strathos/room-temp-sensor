# Room temperature sensor for Home Assistant

Room temperature and relative humidity sensor based on ESP8266 and DHT22. ESP is running Espruino firmware and communication is done via MQTT.

## Requirements

* [esptool.py](https://github.com/espressif/esptool)
* [Node.js](https://nodejs.org/en/)
* [Git](https://git-scm.com/)
* [Espruino IDE Chrome Extension](https://chrome.google.com/webstore/detail/espruino-web-ide/bleoifhkdalbjfbobjackfdifdneehpo?hl=en)

## Components used

* Wemos D1 Mini Pro
* DHT22

## Pin connections

* Wemos RST connected to Wemos D0
* Wemos 5V connected to DHT22 PIN1
* Wemos D1 connected to DHT22 PIN2
* Wemos GND connected to DHT22 PIN4

## Download the repository

Use git to download the repository:

```
git clone
```

Install the dependencies with npm:

```
npm install
```

## Flashing the firmware

Flash the firmware with esptool. Example with Windows. Replace the port with correct one, especially if using macOS or Linux.

Espruino firmware can be downloaded from [Espruino's download page](http://www.espruino.com/Download). The code has been tested with 1v95.

```
pip install esptool
esptool.py --port COM3 erase_flash
esptool.py --port COM3 --baud 230400 write_flash -fm qio -fs 4MB -ff 80m 0x00000 espruino_1v95_esp8266_4mb_combined_4096.bin
```

## Create the source file to be transferred to ESP8266

Configuration for your network and MQTT will be in a separate file, which will be created with Gulp. In the project directory run the following:

```
npm start
```

This will create a file called "config.js" under ./src and also starts watching for changes in the files under that directory. The output file will be created under ./dist/bundle.js. Edit ./src/config.js to match your WiFi and MQTT configuration and save. Then look at the ./dist/bundle.js to verify everything is OK.

## Transfer the code to ESP8266

Open Espruino IDE Chrome Extension and connect to your ESP8266. Either open the bundle.js file or copy-paste its content to the IDE. Push the transfer button, wait for it finish and save the source code so it will be loaded even after a restart by running:

```
save()
```
